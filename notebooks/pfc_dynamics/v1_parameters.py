
parameters = {
    'simulation_time': 10.0,    # total simulation time, sec
    'timestep': 0.002,         # timestep, sec
    'sample_rate': 0.02,    # sampling interval for time series, sec
    'target_rate': 2.0,     # target mean firing rate for I0 vs. U curve
    'U': 0.9,           # baseline neurotransmitter release probability, a.u.
    'D': 0.5,           # internal noise intensity
    'inhibitory_weight': -12,          # E->I synaptic weight
    'excitatory_weight': 30,           # E->E recurrent synaptic weight
    'T': 0.2,           # duration of incoming stimuli, sec
    'C': 5.0,           # amplitude of incoming stimuli, a.u.
    'freq': 4,          # stimulus Poisson rate, Hz
    'tau_r': 0.01,      # readout time constant, sec
    'tau': 0.01,        # firing rate decay time constant, sec
    'tau_n': 0.1,       # noise correlation time constant, sec
    'tau_rec': 0.8,     # time constant for recovery of synaptic resourses, sec
    'N': 100,           # number of neural populations in the network
    'w_inh': 0.1,       #
    'I0_inh': 1.0,      #
    'Ie': [-1.170, -0.917, -0.669, -0.485, -0.485, -0.542, -0.711,
           -0.934, -1.154, -1.398, -1.690, -1.950, -2.256, -2.389,
           -2.543, -2.717, -2.855, -3.033, -3.150]
}
