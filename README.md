To be able to import the *cortexnet* module, you shall create a symbolic link to the ./src subdirectory

    > ln -s ./src cortexnet

and add the directory with the symbolic link to your PYTHONPATH environment variable.