import numpy as np
from tqdm import tqdm
from argparse import Namespace
from cortexnet.parameters import parameters

np.random.seed(0)
pi = np.pi


def initialize_defaults(parameters):
    return Namespace(**parameters)

p = initialize_defaults(parameters)


class RingNetwork:

    ''' Ring network model class '''

    def __init__(self, U, model_parameters=None):

        if model_parameters is not None:
            defaults = vars(p)
            for param_name in defaults:
                if param_name not in model_parameters:
                    model_parameters[param_name] = defaults[param_name]
            self.p = Namespace(**model_parameters)
        else:
            self.p = p

        self.N = self.p.N
        self.U = U

        self.Urange = np.arange(0.05, 0.951, 0.05)
        self.Ierange = self.p.Ie
        self.I0 = self.Ierange[int(self.U / 0.05) - 1]

        self.Inoise = np.zeros(self.N)
        self.x = np.ones(self.N)
        self.m = np.zeros(self.N)
        self.I = np.zeros(self.N)
        self.u = self.U * np.ones(self.N)

        self.activation_type = 'smooth_relu'
        self.interneuron_model = 'static_linear'

        if self.interneuron_model != 'static_linear':
            self.I0_inh = self.p.I0_inh
            self.w_inh = self.p.w_inh

        self.th = np.linspace(-pi, pi, self.N, endpoint=False)
        alpha, beta = np.meshgrid(self.th, self.th)
        self.W_exc = self.p.excitatory_weight * np.cos(alpha - beta) / self.N
        self.WInh = self.p.inhibitory_weight / self.N * np.ones(shape=(self.N, self.N))
        self.W = self.W_exc + self.WInh
        del alpha, beta


    def gain_function(self, input):
        ''' Activation function of neuronal populations '''

        if self.activation_type == 'smooth_relu':
            return np.log(1 + np.exp(input))
        else:
            raise RuntimeError('Non-existent gain function type')


    def inh_gain_function(self, input):
        ''' Activation function of the 
            global interneuron population '''

        if self.activation_type == 'smooth_relu':
            return np.log(1 + np.exp(input))
        else:
            raise RuntimeError('Non-existent gain function type')


    def initialize_integration(self, backend):

        if (backend == 'C') & (self.p.sample_rate != self.p.timestep):
            print('Warning: forcing sample_rate == timestep for backend="C"')
            self.p.sample_rate = self.p.timestep

        self.stime = np.arange(0, self.p.simulation_time, self.p.timestep)
        self.Tsim = int(self.simulation_time / self.p.timestep)

        self.m_series = np.zeros(
            (int(self.simulation_time / self.p.sample_rate), self.N), dtype='float32')
        self.x_series = np.zeros(
            (int(self.simulation_time / self.p.sample_rate), self.N), dtype='float32')
        self.u_series = self.U * np.ones(
            (int(self.simulation_time / self.p.sample_rate), self.N), dtype='float32')

        self.Iex = np.zeros(self.Tsim, dtype='float32')
        self.ThetaEs = np.zeros(self.Tsim, dtype='float32')

        # number of input events, nearly roughly poisson rate*time
        self.Nev = int(self.p.freq * self.simulation_time)
        # times of Poisson events have exponential distribution
        self.inpTimes = (np.random.exponential(1 / self.p.freq, self.Nev) + self.p.T).cumsum() / self.p.timestep
        # cutoff events which occur later than simulation time
        self.inpTimes = np.array(
            self.inpTimes[self.inpTimes < self.Tsim - 2 * self.p.T / self.p.timestep], dtype='int')

        self.Nev = len(self.inpTimes)

        # uniformly distributed angles of input events
        self.inpTheta = np.random.uniform(-pi, pi, self.Nev)

        for t, theta in zip(self.inpTimes, self.inpTheta):
            self.Iex[t:int(t + self.p.T / self.p.timestep)] = self.p.C
            self.ThetaEs[t:int(t + self.p.T / self.p.timestep)] = theta

        self.Nreads = np.linspace(int(self.N / 10), self.N, 10, dtype='int')
        self.readout = np.zeros((self.Tsim, len(self.Nreads)), dtype='complex')
        self.exact_readout = np.zeros(self.Tsim, dtype='complex')

        self.readout_populations = {}
        for idx, n in enumerate(self.Nreads):
            self.readout_populations[idx] = np.random.choice(range(self.N), n,
                                                             replace=None)

        if backend == 'C':

            self.Inoise = np.zeros((self.Tsim, self.N), dtype='float32')

            for t in range(1, self.Tsim):
                self.Inoise[t, :] = self.Inoise[t-1, :] + (-self.Inoise[t-1, :] * self.p.timestep / self.p.tau_n +
                                    self.p.D * np.sqrt(2 * self.p.timestep / self.p.tau_n) *
                                    np.random.randn(self.N))


    def integrate(self, simulation_time=None,
                  check_readouts=True, show_iterations=True,
                  backend='python'):
        ''' Intergrate model ODEs with forward Euler method '''

        if simulation_time is None:
            self.simulation_time = self.p.simulation_time
        else:
            self.simulation_time = simulation_time

        self.initialize_integration(backend=backend)

        if backend == 'C':
            try:
                import cortexnet.ring_binding as ring_binding
            except ImportError:
                raise ImportError('Error: cannot import the C binding module!')

            ring_binding.set_simulation_parameters(self.Tsim, self.p.timestep,
                                               int(self.p.sample_rate / self.p.timestep))
            ring_binding.set_model_parameters(self.N, self.U, self.p.tau,
                                              self.p.tau_rec, self.p.tau_fac, self.p.is_facilitating, self.I0,
                                              self.p.inh_strength, self.p.inh_smooth, self.p.is_divisive)
            ring_binding.initialize_arrays(self.x, self.m, self.u, self.W, self.x_series,
                                           self.m_series, self.u_series, self.Iex, self.ThetaEs, self.Inoise, self.WInh)
            ring_binding.integrate()

            if check_readouts:

                for t in range(self.m_series.shape[0]-1):

                    self.exact_readout[t + 1] = np.sum(np.exp(1j * self.th) * self.m_series[t, :]) / self.N

                    for idx, Nread in enumerate(self.Nreads):

                        R_derivative = np.sum(np.exp(1j * self.th[self.readout_populations[idx]]) *
                                              np.random.poisson(self.m_series[t, self.readout_populations[idx]] * self.p.timestep, Nread))
                        self.readout[t + 1, idx] = self.readout[t, idx] + (-self.readout[t, idx] +
                                                                            (1 / Nread) * R_derivative) * self.p.timestep / self.p.tau_r

        elif backend == 'python':

            if show_iterations:
                iterator = tqdm(range(0, self.Tsim - 1))
            else:
                iterator = range(0, self.Tsim - 1)

            for t in iterator:

                self.Inoise += (-self.Inoise * self.p.timestep / self.p.tau_n +
                                self.p.D * np.sqrt(2 * self.p.timestep / self.p.tau_n) *
                                np.random.randn(self.N))

                self.x += ((1 - self.x) / self.p.tau_rec -
                           self.U * self.x * self.m) * self.p.timestep

                if self.interneuron_model == 'static_linear':

                    inputs = np.dot(self.W + self.p.inhibitory_weight / self.N, self.U * self.x * self.m) + self.I0 + \
                        self.Iex[t] * np.cos(self.th - self.ThetaEs[t]) + self.Inoise

                elif self.interneuron_model == 'static_nonlinear':

                    inputs = np.dot(self.W, self.U * self.x * self.m) + self.I0 + \
                        + self.p.w_inh * self.inh_gain_function(np.dot(0*self.W - self.p.inhibitory_weight / self.N, self.U * self.x * self.m) +
                            self.p.I0_inh) + self.Iex[t] * np.cos(self.th - self.ThetaEs[t]) + self.Inoise

                elif self.interneuron_model == 'dynamic_nonlinear':

                    inh_inputs = np.dot(0*self.W - self.p.inhibitory_weight / self.N, self.U * self.x * self.m) + self.p.I0_inh
                    self.I += (-self.I + self.inh_gain_function(inh_inputs)) * (self.p.timestep / self.p.tau)
                    inputs = np.dot(self.W, self.U * self.x * self.m) + self.I0 + \
                        + self.p.w_inh * self.I + self.Iex[t] * np.cos(self.th - self.ThetaEs[t]) + self.Inoise

                else:
                    raise RuntimeError('Wrong interneuron model type: '
                                       '{}'.format(self.interneuron_model))

                self.m += (-self.m + self.gain_function(inputs)) * (self.p.timestep / self.p.tau)

                if t % int(self.p.sample_rate / self.p.timestep) == 0:
                    self.m_series[int(t / (self.p.sample_rate / self.p.timestep))] = self.m
                    self.x_series[int(t / (self.p.sample_rate / self.p.timestep))] = self.x
                    if self.interneuron_model == 'dynamic_nonlinear':
                        self.I_series[int(t / (self.p.sample_rate / self.p.timestep))] = self.I

                if check_readouts:
                    for idx, Nread in enumerate(self.Nreads):
                        R_derivative = np.sum(np.exp(1j * self.th[self.readout_populations[idx]]) *
                                              np.random.poisson(self.m[self.readout_populations[idx]] * self.p.timestep, Nread))
                        self.readout[t + 1, idx] = self.readout[t, idx] + (-self.readout[t, idx] +
                                                                           (1 / Nread) * R_derivative) * self.p.timestep / self.p.tau_r

        else:
            raise RuntimeError('Error: the "backend" argument can only be equal to "python" or "C"!')


    def error_for_different_readout_size(self, lag=0):
        '''Calculate readout error for sparse readout'''

        error_sparse_readout = np.zeros((len(self.Nreads), self.Nev))

        for i, (t, theta) in enumerate(zip(self.inpTimes, self.inpTheta)):
            for idx, Nread in enumerate(self.Nreads):
                nev = abs(np.angle(self.readout[t + lag:int(t + lag + self.p.T / self.p.timestep), idx]) -
                          self.ThetaEs[t:int(t + self.p.T / self.p.timestep)])
                angle_difference = np.amin([nev, 2 * pi - nev], axis=0)
                error_sparse_readout[idx, i] = angle_difference.mean()

        return error_sparse_readout

    def error_for_exact_readout(self, lag=0):
        '''Calculate readout error for exact readout'''

        error_exact_readout = np.zeros(self.Nev)

        for i, (t, theta) in enumerate(zip(self.inpTimes, self.inpTheta)):
            nev = np.abs(np.angle(self.exact_readout[t + lag:int(t + lag + self.p.T / self.p.timestep)]) -
                         self.ThetaEs[t:int(t + self.p.T / self.p.timestep)])
            angle_difference = np.amin([nev, 2 * pi - nev], axis=0)
            error_exact_readout[i] = angle_difference.mean()

        return error_exact_readout

    def calculate_readout_error(self):

        lags = np.arange(0, int(self.p.T / self.p.timestep), 1, dtype='int')

        return np.array([self.error_for_different_readout_size(lag) for lag in lags]), \
            np.array([self.error_for_exact_readout(lag) for lag in lags])
