#ifndef RING_H_
#define RING_H_

void setSimulationParameters(unsigned Tsim, double timestep, unsigned sample_step);

void setModelParameters(unsigned N, double U, double tau, double tau_rec, 
                        double tau_fac, double is_facilitating, double I0, double inh_strength, double inh_smooth, double is_divisive);

void initializeArrays(double* x, double* m, double* u, double* W, float* x_series,
                      float* m_series, float* u_series, float* Iex, float* ThetaEs, float* readout, double* wInh);

void c_integrate();

#endif /* RING_H_ */