cimport numpy as np
import numpy as np
import cython

cdef extern from "ring.h":
    void setSimulationParameters(unsigned Tsim, double timestep, unsigned sample_step)

    void setModelParameters(unsigned N, double U, double tau, double tau_rec,
                            double tau_fac, double is_facilitating, double I0, double inh_strength, double inh_smooth, double is_divisive)

    void initializeArrays(double* x, double* m, double* u, double* W, float* x_series,
                          float* m_series, float* u_series, float* Iex, float* ThetaEs, float* Inoise, double* wInh)

    void c_integrate()

def set_simulation_parameters(Tsim, timestep, sample_step):
    setSimulationParameters(Tsim, timestep, sample_step)

def set_model_parameters(unsigned N, double U, double tau, double tau_rec,
                         double tau_fac, double is_facilitating, double I0, double inh_strength, double inh_smooth, double is_divisive):

    setModelParameters(N, U, tau, tau_rec, tau_fac, is_facilitating, I0, inh_strength, inh_smooth, is_divisive)

def initialize_arrays(np.ndarray[np.float64_t, ndim=1] x,
                      np.ndarray[np.float64_t, ndim=1] m,
                      np.ndarray[np.float64_t, ndim=1] u,
                      np.ndarray[np.float64_t, ndim=2] W,
                      np.ndarray[np.float32_t, ndim=2] x_series,
                      np.ndarray[np.float32_t, ndim=2] m_series,
                      np.ndarray[np.float32_t, ndim=2] u_series,
                      np.ndarray[np.float32_t, ndim=1] Iex,
                      np.ndarray[np.float32_t, ndim=1] ThetaEs,
                      np.ndarray[np.float32_t, ndim=2] Inoise,
                      np.ndarray[np.float64_t, ndim=2] WInh):

    initializeArrays(<double*> x.data, <double*> m.data, <double*> u.data, <double*> W.data,
                     <float*> x_series.data, <float*> m_series.data, <float*> u_series.data,
                     <float*> Iex.data, <float*> ThetaEs.data, <float*> Inoise.data, <double*> WInh.data)

def integrate():
    c_integrate()
