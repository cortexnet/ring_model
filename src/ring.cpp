#include <cmath>
#include "ring.h"
#include </usr/intel/pkgs/mkl_libs-64/10.1.0.015/include/mkl.h>

#define SMOOTH 1.0 // smoothness of gain function
#define PI 3.14159

namespace ring{
    double* wArr; // 2D matrix of weights stored in row major order
    double* wInh;
    double* xArr;
    double* mArr;
    double* uArr;
    float* xRes;
    float* mRes;
    float* uRes;
    float* ThetaEs;
    float* Iex;
    float* Inoise;
    double* mRight; // multiplication of mArr to xArr
    double* mxOld; // multiplication of mArr to xArr
    double* hInh;

    unsigned N;
    double tau_rec;
    double tau_fac;
    double is_facilitating;
    double is_divisive;
    double inh_strength;
    double inh_smooth;
    double U;
    double I0;
    double tau;
    unsigned Tsim;
    double timestep;
    unsigned sample_step;
}

using namespace ring;

void setSimulationParameters(unsigned Tsim, double timestep, unsigned sample_step){
    ring::Tsim = Tsim;
    ring::timestep = timestep;
    ring::sample_step = sample_step;
}

void setModelParameters(unsigned N, double U, double tau, double tau_rec, double tau_fac, 
                        double is_facilitating, double I0, double inh_strength, double inh_smooth, double is_divisive){
    ring::N = N;
    ring::U = U;
    ring::tau = tau;
    ring::tau_rec = tau_rec;
    ring::tau_fac = tau_fac;
    ring::is_facilitating = is_facilitating;
    ring::is_divisive = is_divisive;
    ring::I0 = I0;
    ring::inh_strength = inh_strength;
    ring::inh_smooth = inh_smooth;
}

void initializeArrays(double* x, double* m, double* u, double* W, float* x_series, float* m_series, 
                      float* u_series, float* Iex, float* ThetaEs, float* Inoise, double* wInh){
    ring::xArr = x;
    ring::mArr = m;
    ring::uArr = u;
    ring::wArr = W;
    ring::wInh = wInh;
    ring::xRes = x_series;
    ring::mRes = m_series;
    ring::uRes = u_series;
    ring::Inoise = Inoise;
    ring::ThetaEs = ThetaEs;
    ring::Iex = Iex;
    ring::mxOld = new double[ring::N];
    ring::mRight = new double[ring::N];
    ring::hInh = new double[ring::N];
    for (unsigned i = 0; i < ring::N; i++){
        ring::mxOld[i] = ring::mArr[i]*ring::xArr[i]*ring::uArr[i];
    }
}


float gain_function(double input){
    double threshold = 500.0;
    if (input < threshold){
        return SMOOTH*(log(1.0 + exp(input/SMOOTH)));
    } else {
        return input;
    }
}

float inh_factor(double input, double inh_strength, double smooth, double is_divisive){
    double threshold = -300.0;
    if (is_divisive == 1){
        if (input > threshold){
            return 1 / (1 + exp(- (input + inh_strength) / smooth));
        } else {
            return 0.0;
        }
    } else {
        return 1.0;
    }
}

void c_integrate(){

    for (unsigned t = 0; t < Tsim; t++){
        
        cblas_dgemv(CblasRowMajor, CblasNoTrans, N, N, 1., wArr, N,
                    mxOld, 1, 0.0, mRight, 1);
        cblas_dgemv(CblasRowMajor, CblasNoTrans, N, N, U, wInh, N,
                    mxOld, 1, 0.0, hInh, 1);
        
        for (unsigned i = 0; i < N; i++){
            
            uArr[i] += is_facilitating * ((U - uArr[i])/tau_fac + U * (1 - uArr[i]) * mArr[i]) * timestep;

            xArr[i] += ((1.0 - xArr[i])/tau_rec - mxOld[i]) * timestep;

            mArr[i] += (-mArr[i] + inh_factor(hInh[i], inh_strength, inh_smooth, is_divisive) * gain_function(mRight[i] + I0 + Inoise[N*t + i] + 
                            Iex[t] * cos(-PI + 2*PI*i/N - ThetaEs[t]))) * timestep / tau;

            if ((t % sample_step) == 0){
               mRes[N*t/sample_step + i] = mArr[i];
               xRes[N*t/sample_step + i] = xArr[i];
               uRes[N*t/sample_step + i] = uArr[i];
           }
       }

       for (unsigned i = 0; i < N; i++){
           mxOld[i] = mArr[i] * xArr[i] * uArr[i];
       }

    }

}