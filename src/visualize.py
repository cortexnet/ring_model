import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pylab as plt
from argparse import Namespace
from matplotlib.gridspec import GridSpec

from cortexnet.parameters import parameters

mpl.rcParams.update({'font.size': 16})


def plot_readout_dynamics(p, network):

    plt.figure(figsize=(4 * 2.5, 3 * 2.5))

    gs = GridSpec(3, 2, height_ratios=[1, 1, 2], width_ratios=[30, 1])
    gs.update(wspace=0.1, hspace=0.1)
    axM = plt.subplot(gs[0, 0])
    axSpec = plt.subplot(gs[1, 0], sharex=axM)
    axAngle = plt.subplot(gs[2, 0], sharex=axM)

    axCbar = plt.subplot(gs[:, 1])

    egg = axM.pcolormesh(np.arange(0, p.simulation_time, p.sample_rate),
                         network.th * 360 / (4 * np.pi), network.m_series.T)
    spam = plt.colorbar(egg, cax=axCbar)
    axCbar.set_title('m[Hz]')
    axM.set_ylim([-90, 90])
    axM.set_ylabel(r'$\theta$')
    plt.setp(axM.get_xticklabels(), visible=False)
    axM.set_title('$U={}\quad I_0={:.2f}$'.format(network.U, network.I0))

    axSpec.plot(network.stime,
                abs(network.exact_readout),
                label='exact readout')
    axSpec.plot(network.stime, abs(network.readout[:, 3] * network.Nreads[3]),
                label='sparse readout (%i subpopulations)' % network.Nreads[3])
    axSpec.set_ylim([0, 16])
    plt.setp(axSpec.get_xticklabels(), visible=False)
    axSpec.set_ylabel(r'$|R|$')
    axSpec.legend(fontsize=16., loc='upper right')

    axAngle.plot(network.stime, np.angle(network.exact_readout)
                 * 360 / (4 * np.pi), label='exact readout')
    axAngle.plot(network.stime, np.angle(
        network.readout[:, 3]) * 360 / (4 * np.pi), label='sparse readout')
    axAngle.hlines(network.inpTheta * 360 / (4 * np.pi), network.inpTimes * p.timestep,
                   network.inpTimes * p.timestep + p.T, 'C3', lw=5.)
    axAngle.set_ylim([-55, 55])
    axAngle.legend(fontsize=16., loc='lower right')

    axAngle.set_ylabel(r'$angle(R)$')
    axAngle.set_xlabel('Time[s]')
    axAngle.set_xlim((4.59, 5.81))
    plt.savefig('v1_dynamics_U_{}.png'.format(network.U), dpi=260.)

    plt.figure(figsize=(4 * 2.3, 3 * 2.3))
    plt.plot(network.stime, np.angle(network.exact_readout) * 360 / (4 * np.pi),
             label='exact readout')
    plt.plot(network.stime, np.angle(network.readout[:, 3]) * 360 / (4 * np.pi),
             label='sparse readout')
    plt.hlines(network.inpTheta * 360 / (4 * np.pi), network.inpTimes * p.timestep,
               network.inpTimes * p.timestep + p.T, 'C3', linewidth=2.)
    plt.ylim([-90, 90])
    plt.legend(fontsize=16., loc='upper right')
    plt.xlim((5.56, 5.8))
    plt.ylim((-11.0, 31.0))
    plt.xlabel('Time[s]')
    plt.ylabel('angle(R)')


def visualize_many_runs(save_figures=False):

    p = Namespace(**parameters)

    SimTime = p.simulation_time
    C = p.C
    dNread = 20
    freq = p.freq
    T = p.T
    N = p.N
    Mm = p.target_rate

    Nreads = np.arange(dNread, N + 1, dNread, dtype='int')
    Urange = np.arange(0.05, 0.951, 0.05)

    folderName = 'res_h_0.002_D_2.0_freq_{:.1f}_T_{:.2f}_m_{:.1f}/'.format(freq, T, Mm)
    name = folderName + 'U_{:.2f}_C_{:.1f}_N_{:d}_SimTime_{:d}.npy'
    nameExactR = folderName + 'U_{:.2f}_C_{:.1f}_N_{:d}_SimTime_{:d}_exactPV.npy'

    errR = np.load(name.format(Urange[0], C, N, SimTime))
    arrShape = len(Urange), np.shape(errR)[0], np.shape(errR)[1], np.shape(errR)[2]
    errR = np.zeros(arrShape)

    arrShape = len(Urange), np.shape(errR)[1], np.shape(errR)[3]
    errExactR = np.zeros(arrShape)

    for idx, u in enumerate(Urange):
        er = np.load(name.format(u, C, N, SimTime))
        errR[idx] = er
        er = np.load(nameExactR.format(u, C, N, SimTime))
        errExactR[idx] = er

    meanErrR = np.mean(errR, axis=3)
    minLags = np.argmin(meanErrR, axis=1)
    minLagErr = np.amin(meanErrR, axis=1)

    meanErrExactR = np.mean(errExactR, axis=2)
    minLagsExact = np.argmin(meanErrExactR, axis=1)
    minLagErrExact = np.amin(meanErrExactR, axis=1)

    minErrNread = np.zeros(len(Nreads))
    NreadsForPlot = [20, 40, 80, 200]
    for idx, Nread in enumerate(Nreads):
        errs = minLagErr[:, int(Nread/dNread - 1)]*360/(4*np.pi)

        minErrNread[idx] = Urange[np.argmin(errs)]
        if Nread in NreadsForPlot:
            plt.figure(1, figsize=(4*2.5, 3*2.5))
            plt.plot(Urange, errs, '-o', label=r'$N_{{read}}={}$'.format(Nread))

            plt.figure(2, figsize=(4*2.5, 3*2.5))
            plt.plot(Urange, minLags[:, int(Nread/dNread - 1)]*2., '-o',
                     label=r'$N_{{read}}={}$'.format(Nread))

    plt.figure(1, figsize=(4*2.5, 3*2.5))
    plt.plot(Urange, minLagErrExact*360/(4*np.pi), '--o', label='exact readout')
    plt.title('$C={}$'.format(C))
    plt.legend(loc='upper right', fontsize=14.0)
    plt.xlabel('U')
    plt.ylabel('$Error[degree \; ^{\circ}$]')
    if save_figures:
        plt.savefig('Freq_{}_C_{}_Tsim_{}_mean_{}'
                    '_N_{}.png'.format(freq, C, SimTime, Mm, N), dpi=260.)

    plt.figure(2, figsize=(4*2.5, 3*2.5))
    plt.plot(Urange, minLagsExact*2, '--o', label='exact readout')
    plt.title('$C={}$'.format(C))
    plt.legend(loc='upper right', fontsize=14.0)
    plt.xlabel('U')
    plt.ylabel('$lag[ms]$')
    if save_figures:
        plt.savefig('lag_Freq_{}_C_{}_Tsim_{}_mean_{}'
                    '_N_{}.png'.format(freq, C, SimTime, Mm, N), dpi=260.)

    plt.figure(3, figsize=(4*2.5, 3*2.5))
    plt.plot(Nreads, minErrNread, '-o', label='$C= {}$'.format(C))
    plt.xlabel(r'$N_{read}$')
    plt.ylabel(r'U for minimal error')
    if save_figures:
        plt.savefig('U_for_min_Freq_{}_C_{}_Tsim_{}_mean_'
                    '{}_N_{}.png'.format(freq, C, SimTime, Mm, N), dpi=260.)
