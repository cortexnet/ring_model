
parameters = {
    'simulation_time': 10.0,    # total simulation time, sec
    'timestep': 0.002,         # timestep, sec
    'sample_rate': 0.02,    # sampling interval for time series, sec
    'target_rate': 2.0,     # target mean firing rate for I0 vs. U curve
    'U': 0.9,           # baseline neurotransmitter release probability, a.u.
    'D': 0.5,           # internal noise intensity
    'inhibitory_weight': -12,          # E->I synaptic weight
    'excitatory_weight': 30,           # E->E recurrent synaptic weight
    'T': 0.2,           # duration of incoming stimuli, sec
    'C': 5.0,           # amplitude of incoming stimuli, a.u.
    'freq': 4,          # stimulus Poisson rate, Hz
    'tau_r': 0.01,      # readout time constant, sec
    'tau': 0.01,        # firing rate decay time constant, sec
    'tau_n': 0.1,       # noise correlation time constant, sec
    'tau_rec': 0.8,     # time constant for recovery of synaptic resourses, sec
    'tau_fac': 3.0,
    'N': 100,           # number of neural populations in the network
    'is_facilitating': True,
    'inh_strength': 50,
    'inh_smooth': 5.,
    'is_divisive': True,
    'Ie': [-1.5] * 19,
}
