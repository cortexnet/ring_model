import os
import sys
import numpy as np

from argparse import Namespace

from cortexnet import cortexnetwork, determine_U_I0_curve
from cortexnet.visualize import plot_readout_dynamics
from cortexnet.parameters import parameters

np.random.seed(0)

pi = np.pi

parameters['Tsim'] = int(parameters['simulation_time'] /
                         parameters['timestep'])

p = Namespace(**parameters)


def run_single_realization(generate_plot=True):

    network = cortexnetwork(U=p.U)
    network.integrate()

    if generate_plot:
        plot_readout_dynamics(p, network)

    readout_error, exact_readout_error = network.calculate_readout_error()

    sec2ms = 1000
    # readout_error is a 3d array of shape (lags, Nreads, Number of stimulus)
    # we are looking for an optimal value of time lag
    optimal_time_lags = np.argmin(np.mean(readout_error, axis=2),
                                  axis=0) * p.timestep * sec2ms
    errors_for_optimal_lag = np.amin(
        np.mean(readout_error, axis=2), axis=0) * 360 / (4 * pi)

    print('Lags for minimal error {} ms'.format(optimal_time_lags))
    print('Minimal errors {}'.format(errors_for_optimal_lag))

    optimal_lag_exact_readout = np.argmin(
        np.mean(
            exact_readout_error,
            axis=-1),
        axis=0) * p.timestep * sec2ms
    optimal_error_exact_readout = np.amin(
        np.mean(exact_readout_error, axis=-1), axis=0) * 360 / (4 * pi)

    print('Lag for minimal error {} ms (exact PV)'.format(
        optimal_lag_exact_readout))
    print('Minimal error {} (exact PV)'.format(optimal_error_exact_readout))


def run_different_U_realizations(custom_U_range=None):

    result_directory = 'res_h_{}_D_{}_freq_{:.1f}_T'\
                   '_{:.2f}_m_{:.1f}/'.format(p.timestep, p.D, p.freq,
                                              p.T, p.target_rate)

    if not os.path.exists(result_directory):
        os.makedirs(result_directory)

    network = cortexnetwork(U=0.1)

    if custom_U_range is not None:
        network.Urange = custom_U_range
        network.Ierange = [determine_U_I0_curve(U)[2] for U
                           in network.Urange]

    for U, I0 in zip(network.Urange, network.Ierange):

        network.U = U
        network.integrate()
        print('Calculating for U: {}'.format(U))

        readout_error, exact_readout_error = network.calculate_readout_error()

        np.save(result_directory + 'U_{:.2f}_C_{:.1f}_N_{:n}_'
                'simulation_time_{:n}.npy'.format(U, p.C, p.N, p.simulation_time), readout_error)
        np.save(result_directory + 'U_{:.2f}_C_{:.1f}_N_{:n}_'
                'simulation_time_{:n}_exactPV.npy'.format(U, p.C, p.N, p.simulation_time), exact_readout_error)


def run_with_U_value_from_command_line():

    result_directory = 'res_h_{}_D_{}_freq_{:.1f}_T'\
                   '_{:.2f}_m_{:.1f}/'.format(p.timestep, p.D, p.freq,
                                              p.T, p.target_rate)

    if not os.path.exists(result_directory):
        os.makedirs(result_directory)

    network = cortexnetwork(U=0.1)

    U = float(sys.argv[1])
    I0 = network.Ierange[int(U / 0.05) - 1]

    network.U = U
    network.I0 = I0
    network.integrate(U)

    readout_error, exact_readout_error = network.calculate_readout_error()
    np.save(result_directory + 'U_{:.2f}_C_{:.1f}_N_{:n}_'
            'simulation_time_{:n}.npy'.format(U, p.C, p.N, p.simulation_time), readout_error)
    np.save(result_directory + 'U_{:.2f}_C_{:.1f}_N_{:n}_'
            'simulation_time_{:n}_exactPV.npy'.format(U, p.C, p.N, p.simulation_time), exact_readout_error)


def calculate_I0_vs_U_curve(trials=100):

    result_directory = 'res_h_{}_D_{}_freq_{:.1f}_T'\
                   '_{:.2f}_m_{:.1f}/'.format(p.timestep, p.D, p.freq,
                                              p.T, p.target_rate)

    if not os.path.exists(result_directory):
        os.makedirs(result_directory)

    Urange, Ierange, I0 = determine_U_I0_curve(U=0.1)

    fname = 'U_Iex_simulation_time_{:.1f}_h_{:.4f}_D_{:.1f}_N_'\
            '{:n}_eps_{:.3f}_m_{:.1f}.npy'

    activity_tolerance = .01
    target_firing_rate = p.target_rate

    Imax = 15.0
    Imin = -15.0
    Ilow, Ihigh = Imin, Imax

    Ierange = np.zeros_like(Urange) + np.nan

    for idx, U in enumerate(Urange):
        print('Performing calculations for U = {}'.format(U))

        network = cortexnetwork(U=U)

        for j in range(trials):

            network.integrate(check_readouts=False,
                              show_iterations=False)

            calculated_firing_rate = network.m_series.mean()

            if abs(target_firing_rate - calculated_firing_rate) < activity_tolerance:
                Ilow, Ihigh = Imin, Imax
                Ierange[idx] = network.I0
                break
            if (calculated_firing_rate - target_firing_rate) < 0:
                Ilow = network.I0
            else:
                Ihigh = network.I0
            network.I0 = (Ihigh + Ilow) * 0.5

        print('%f %f %f' % (U, network.I0, network.m_series.mean()))

    np.save(fname.format(p.simulation_time, p.timestep, p.D, p.N,
                         activity_tolerance, target_firing_rate), Ierange)


if __name__ == '__main__':

    run_single_realization()
